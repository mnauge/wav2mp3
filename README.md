# wav2mp3
_Michael Nauge Université de Poitiers_

wav2mp3 est un micro programme utilisant ffmpeg pour réaliser du transcodage de fichiers audio Wav vers Mp3.

Il suffit de préciser le chemin du dossier contenant tous les fichiers Wav à convertir (pathWavIn="...")
et le chemin du dossier devant accueillir les fichiers Mp3 générés (pathMp3Out= "...").

Si après une première conversion nous ajoutons de nouveaux fichiers Wav,
puis que nous relançons le programme,
il n'y a que les nouveaux fichiers qui seront convertit.

Il n'y a pas d'écrasement/reconversion des fichiers déjà traités.

Si le programme ne fonctionne pas il faut probablement ajouter le chemin vers ffmpeg dans les variables d'environnements systèmes.

J'ai ajouté la version binaire compilé pour windows de ffmpeg. 

 