# -*- coding: utf-8 -*-
"""
Created on Thu Aug 20 15:52:08 2020

@author: Michael Nauge Université de Poitiers
"""

import glob
import os
import ntpath

pathWavIn = "wav/"

pathMp3Out= "mp3/"


def convertWav2mp3(pathIn, pathOut):
    
    #get list of all wav files in specified directory
    listFilewav = glob.glob(pathWavIn+"*.wav")
    
    for filewav in listFilewav:
        #extract only filename (not all the path)
        filenameWav = ntpath.basename(filewav)
        #generate pathfile for mp3 output
        pathFilenameMp3 = pathOut+filenameWav[:-3]+"mp3"
        
        #if wav is already exist in mp3 directory
        #do not convert again
        #warning : it is only based on filename
        if not os.path.isfile(pathFilenameMp3):
            print(pathFilenameMp3+" : need conversion")

            #call ffmpeg in commandline
            command = f"ffmpeg -i {filewav} -vn -ar 44100 -ac 2 -b:a 192k {pathFilenameMp3}"
            os.system(command)
            print(pathFilenameMp3+" : done")

        else:
            print(pathFilenameMp3+" : already exist")

    
convertWav2mp3(pathWavIn, pathMp3Out)